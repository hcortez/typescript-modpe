declare global {
    class ModPE {
        static dumpVtable(par1: string, par2: number);
        static getBytesFromTexturePack(par1: string);
        static getI18n(par1: string);
        static getLanguage();
        static getMinecraftVersion(): string;
        static langEdit(par1: string, par2: string);
        static leaveGame();
        static log(par1: string);
        static openInputStreamFromTexturePack(par1: string);
        static overrideTexture(par1: string, par2: string);
        static readData(par1: string);
        static removeData(par1: string);
        static resetFov();
        static resetImages();
        static saveData(par1: string, par2: string);
        static selectLevel(par1: string);
        static setCamera(par1: any);
        static setFoodItem(par1: number, par2: string, par3: number, par4: number, par5: string, par6: number);
        static setFov(par1: number);
        static setGameSpeed(par1: number);
        static setGuiBlocks(par1: string);
        static setItem(par1: number, par2: string, par3: number, par4: string, par5: number);
        static setItems(par1: string);
        static setTerrain(par1: string);
        static setUiRenderDebug(par1: boolean);
        static showTipMessage(par1: string);
        static takeScreenshot(par1: string);
    }

    class Level {
        static addParticle(par1: any, par2: number, par3: number, par4: number, par5: number, par6: number, par7: number, par8: number);
        static biomeIdToName(par1: number);
        static canSeeSky(par1: number, par2: number, par3: number);
        static destroyBlock(par1: number, par2: number, par3: number, par4: boolean);
        static dropItem(par1: number, par2: number, par3: number, par4: number, par5: number, par6: number, par7: number);
        static explode(par1: number, par2: number, par3: number, par4: number, par5: boolean, par6: boolean, par7: number);
        static getAddress();
        static getBiome(par1: number, par2: number);
        static getBiomeName(par1: number, par2: number);
        static getBrightness(par1: number, par2: number, par3: number);
        static getChestSlot(par1: number, par2: number, par3: number, par4: number);
        static getChestSlotCount(par1: number, par2: number, par3: number, par4: number);
        static getChestSlotCustomName(par1: number, par2: number, par3: number, par4: number);
        static getChestSlotData(par1: number, par2: number, par3: number, par4: number);
        static getData(par1: number, par2: number, par3: number);
        static getDifficulty();
        static getFurnaceSlot(par1: number, par2: number, par3: number, par4: number);
        static getFurnaceSlotCount(par1: number, par2: number, par3: number, par4: number);
        static getFurnaceSlotData(par1: number, par2: number, par3: number, par4: number);
        static getGameMode();
        static getGrassColor(par1: number, par2: number);
        static getLightningLevel();
        static getRainLevel();
        static getSignText(par1: number, par2: number, par3: number, par4: number);
        static getSpawnerEntityType(par1: number, par2: number, par3: number);
        static getTile(par1: number, par2: number, par3: number);
        static getTime();
        static getWorldDir();
        static getWorldName();
        static playSound(par1: number, par2: number, par3: number, par4: string, par5: number, par6: number);
        static playSoundEnt(par1: any, par2: string, par3: number, par4: number);
        static setChestSlot(par1: number, par2: number, par3: number, par4: number, par5: number, par6: number, par7: number);
        static setChestSlotCustomName(par1: number, par2: number, par3: number, par4: number, par5: string);
        static setDifficulty(par1: number);
        static setFurnaceSlot(par1: number, par2: number, par3: number, par4: number, par5: number, par6: number, par7: number);
        static setGameMode(par1: number);
        static setGrassColor(par1: number, par2: number, par3: number);
        static setLightningLevel(par1: number);
        static setNightMode(par1: boolean);
        static setRainLevel(par1: number);
        static setSignText(par1: number, par2: number, par3: number, par4: number, par5: string);
        static setSpawn(par1: number, par2: number, par3: number);
        static setSpawnerEntityType(par1: number, par2: number, par3: number, par4: number);
        static setTile(par1: number, par2: number, par3: number, par4: number, par5: number);
        static setTime(par1: number);
        static spawnChicken(par1: number, par2: number, par3: number, par4: string);
        static spawnCow(par1: number, par2: number, par3: number, par4: string);
        static spawnMob(par1: number, par2: number, par3: number, par4: number, par5: string);
    }

    class Player {
        static addExp(par1: number);
        static addItemCreativeInv(par1: number, par2: number, par3: number);
        static addItemInventory(par1: number, par2: number, par3: number);
        static canFly();
        static clearInventorySlot(par1: number);
        static enchant(par1: number, par2: number, par3: number);
        static getArmorSlot(par1: number);
        static getArmorSlotDamage(par1: number);
        static getCarriedItem();
        static getCarriedItemCount();
        static getCarriedItemData();
        static getDimension();
        static getEnchantments(par1: number);
        static getEntity();
        static getExhaustion();
        static getExp();
        static getHunger();
        static getInventorySlot(par1: number);
        static getInventorySlotCount(par1: number);
        static getInventorySlotData(par1: number);
        static getItemCustomName(par1: number);
        static getLevel();
        static getName(par1: any);
        static getPointedBlockData();
        static getPointedBlockId();
        static getPointedBlockSide();
        static getPointedBlockX();
        static getPointedBlockY();
        static getPointedBlockZ();
        static getPointedEntity();
        static getPointedVecX();
        static getPointedVecY();
        static getPointedVecZ();
        static getSaturation();
        static getScore();
        static getSelectedSlotId();
        static getX();
        static getY();
        static getZ();
        static isFlying();
        static isPlayer(par1: any);
        static setArmorSlot(par1: number, par2: number, par3: number);
        static setCanFly(par1: boolean);
        static setExhaustion(par1: number);
        static setExp(par1: number);
        static setFlying(par1: boolean);
        static setHealth(par1: number);
        static setHunger(par1: number);
        static setInventorySlot(par1: number, par2: number, par3: number, par4: number);
        static setItemCustomName(par1: number, par2: string);
        static setLevel(par1: number);
        static setSaturation(par1: number);
        static setSelectedSlotId(par1: number);
    }

    class Entity {
        static addEffect(par1: any, par2: number, par3: number, par4: number, par5: boolean, par6: boolean);
        static getAll();
        static getAnimalAge(par1: any);
        static getArmor(par1: any, par2: number);
        static getArmorCustomName(par1: any, par2: number);
        static getArmorDamage(par1: any, par2: number);
        static getEntityTypeId(par1: any);
        static getExtraData(par1: any, par2: string);
        static getHealth(par1: any);
        static getItemEntityCount(par1: any);
        static getItemEntityData(par1: any);
        static getItemEntityId(par1: any);
        static getMaxHealth(par1: any);
        static getMobSkin(par1: any);
        static getNameTag(par1: any);
        static getPitch(par1: any);
        static getRenderType(par1: any);
        static getRider(par1: any);
        static getRiding(par1: any);
        static getTarget(par1: any);
        static getUniqueId(par1: any);
        static getVelX(par1: any);
        static getVelY(par1: any);
        static getVelZ(par1: any);
        static getX(par1: any);
        static getY(par1: any);
        static getYaw(par1: any);
        static getZ(par1: any);
        static isSneaking(par1: any);
        static remove(par1: any);
        static removeAllEffects(par1: any);
        static removeEffect(par1: any, par2: number);
        static rideAnimal(par1: any, par2: any);
        static setAnimalAge(par1: any, par2: number);
        static setArmor(par1: any, par2: number, par3: number, par4: number);
        static setArmorCustomName(par1: any, par2: number, par3: string);
        static setCape(par1: any, par2: string);
        static setCarriedItem(par1: any, par2: number, par3: number, par4: number);
        static setCollisionSize(par1: any, par2: number, par3: number);
        static setExtraData(par1: any, par2: string, par3: string);
        static setFireTicks(par1: any, par2: number);
        static setHealth(par1: any, par2: number);
        static setImmobile(par1: any, par2: boolean);
        static setMaxHealth(par1: any, par2: number);
        static setMobSkin(par1: any, par2: string);
        static setNameTag(par1: any, par2: string);
        static setPosition(par1: any, par2: number, par3: number, par4: number);
        static setPositionRelative(par1: any, par2: number, par3: number, par4: number);
        static setRenderType(par1: any, par2: any);
        static setRot(par1: any, par2: number, par3: number);
        static setSneaking(par1: any, par2: boolean);
        static setTarget(par1: any, par2: any);
        static setVelX(par1: any, par2: number);
        static setVelY(par1: any, par2: number);
        static setVelZ(par1: any, par2: number);
        static spawnMob(par1: number, par2: number, par3: number, par4: number, par5: string);
    }

    class Item {
        static addCraftRecipe(par1: number, par2: number, par3: number, par4: any);
        static addFurnaceRecipe(par1: number, par2: number, par3: number);
        static addShapedRecipe(par1: number, par2: number, par3: number, par4: any, par5: any);
        static defineArmor(par1: number, par2: string, par3: number, par4: string, par5: string, par6: number, par7: number, par8: number);
        static defineThrowable(par1: number, par2: string, par3: number, par4: string, par5: number);
        static getCustomThrowableRenderType(par1: number);
        static getMaxDamage(par1: number);
        static getMaxStackSize(par1: number);
        static getName(par1: number, par2: number, par3: boolean);
        static getTextureCoords(par1: number, par2: number);
        static getUseAnimation(par1: number);
        static internalNameToId(par1: string);
        static isValidItem(par1: number);
        static setCategory(par1: number, par2: number);
        static setEnchantType(par1: number, par2: number, par3: number);
        static setHandEquipped(par1: number, par2: boolean);
        static setMaxDamage(par1: number, par2: number);
        static setProperties(par1: number, par2: any);
        static setStackedByData(par1: number, par2: boolean);
        static setUseAnimation(par1: number, par2: number);
        static translatedNameToId(par1: string);
    }

    class Block {
        static defineBlock(par1: number, par2: string, par3: any, par4: any, par5: any, par6: any);
        static defineLiquidBlock(par1: number, par2: string, par3: any, par4: any);
        static getAllBlockIds();
        static getDestroyTime(par1: number, par2: number);
        static getFriction(par1: number, par2: number);
        static getRenderLayer(par1: number);
        static getRenderType(par1: number);
        static getTextureCoords(par1: number, par2: number, par3: number);
        static setColor(par1: number, par2: any);
        static setDestroyTime(par1: number, par2: number);
        static setExplosionResistance(par1: number, par2: number);
        static setFriction(par1: number, par2: number);
        static setLightLevel(par1: number, par2: number);
        static setLightOpacity(par1: number, par2: number);
        static setRedstoneConsumer(par1: number, par2: boolean);
        static setRenderLayer(par1: number, par2: number);
        static setRenderType(par1: number, par2: number);
        static setShape(par1: number, par2: number, par3: number, par4: number, par5: number, par6: number, par7: number, par8: number);
    }

    class Server {
        static getAddress();
        static getAllPlayerNames();
        static getAllPlayers();
        static getPort();
        static joinServer(par1: string, par2: number);
        static sendChat(par1: string);
    }

    function addItemInventory(par1: number, par2: number, par3: number);
    function bl_setMobSkin(par1: any, par2: string);
    function bl_spawnMob(par1: number, par2: number, par3: number, par4: number, par5: string);
    function clientMessage(par1: string);
    function explode(par1: number, par2: number, par3: number, par4: number, par5: boolean);
    function getCarriedItem();
    function getLevel();
    function getPitch(par1: any);
    function getPlayerEnt();
    function getPlayerX();
    function getPlayerY();
    function getPlayerZ();
    function getTile(par1: number, par2: number, par3: number);
    function getYaw(par1: any);
    function preventDefault();
    function print(par1: string);
    function rideAnimal(par1: any, par2: any);
    function setNightMode(par1: boolean);
    function setPosition(par1: any, par2: number, par3: number, par4: number);
    function setPositionRelative(par1: any, par2: number, par3: number, par4: number);
    function setRot(par1: any, par2: number, par3: number);
    function setTile(par1: number, par2: number, par3: number, par4: number, par5: number);
    function setVelX(par1: any, par2: number);
    function setVelY(par1: any, par2: number);
    function setVelZ(par1: any, par2: number);
    function spawnChicken(par1: number, par2: number, par3: number, par4: string);
    function spawnCow(par1: number, par2: number, par3: number, par4: string);
    function spawnPigZombie(par1: number, par2: number, par3: number, par4: number, par5: string);

    function chatHook(str: string);
    /**
     * can use preventDefault()
     */
    function continueDestroyBlock(x: number, y: number, z: number, side: number, progress: number);
    /**
     * can use preventDefault()
     */
    function destroyBlock(x, y, z, side);

    function projectileHitEntityHook(projectile, targetEntity);
    function eatHook(hearts, saturationRatio);
    function entityAddedHook(entity);
    /**
     * can use preventDefault()
     */
    function entityHurtHook(attacker, victim, halfhearts);
    function entityRemovedHook(entity);
    /**
     * can use preventDefault()
     */
    function explodeHook(entity, x, y, z, power, onFire);
    /**
     * can use preventDefault()
     */
    function serverMessageReceiveHook(str);
    /**
     * can use preventDefault()
     */
    function deathHook(attacker, victim);
    /**
     * can use preventDefault()
     */
    function playerAddExpHook(player, experienceAdded);
    /**
     * can use preventDefault()
     */
    function playerExpLevelChangeHook(player, levelsAdded);
    function redstoneUpdateHook(x, y, z, newCurrent, someBooleanIDontKnow, blockId, blockData);
    function screenChangeHook(screenName);
    function newLevel();
    /**
     * can use preventDefault()
     */
    function startDestroyBlock(x, y, z, side);
    function projectileHitBlockHook(projectile, blockX, blockY, blockZ, side);
    function modTick();
    /**
     * can use preventDefault()
     */
    function useItem(x, y, z, itemid, blockid, side, itemDamage, blockDamage);

    class ChatColor {
        static readonly AQUA: string;
        static readonly BEGIN: string;
        static readonly BLACK: string;
        static readonly BLUE: string;
        static readonly BOLD: string;
        static readonly DARK_AQUA: string;
        static readonly DARK_BLUE: string;
        static readonly DARK_GRAY: string;
        static readonly DARK_GREEN: string;
        static readonly DARK_PURPLE: string;
        static readonly DARK_RED: string;
        static readonly GOLD: string;
        static readonly GRAY: string;
        static readonly GREEN: string;
        static readonly LIGHT_PURPLE: string;
        static readonly RED: string;
        static readonly RESET: string;
        static readonly WHITE: string;
        static readonly YELLOW: string;
    }

    class ItemCategory {
        static readonly DECORATION;
        static readonly FOOD;
        static readonly INTERNAL;
        static readonly MATERIAL;
        static readonly TOOL;
    }

    class ParticleType {
        static readonly angryVillager;
        static readonly bubble;
        static readonly carrotboost;
        static readonly cloud;
        static readonly crit;
        static readonly dripLava;
        static readonly dripWater;
        static readonly enchantmenttable;
        static readonly fallingDust;
        static readonly flame;
        static readonly happyVillager;
        static readonly heart;
        static readonly hugeexplosion;
        static readonly hugeexplosionSeed;
        static readonly ink;
        static readonly itemBreak;
        static readonly largeexplode;
        static readonly lava;
        static readonly mobFlame;
        static readonly note;
        static readonly portal;
        static readonly rainSplash;
        static readonly redstone;
        static readonly slime;
        static readonly smoke;
        static readonly smoke2;
        static readonly snowballpoof;
        static readonly spell;
        static readonly spell2;
        static readonly spell3;
        static readonly splash;
        static readonly suspendedTown;
        static readonly terrain;
        static readonly waterWake;
        static readonly witchspell;
    }

    class EntityType {
        static readonly ARROW;
        static readonly BAT;
        static readonly BLAZE;
        static readonly BOAT;
        static readonly CAVE_SPIDER;
        static readonly CHICKEN;
        static readonly COW;
        static readonly CREEPER;
        static readonly EGG;
        static readonly ENDERMAN;
        static readonly EXPERIENCE_ORB;
        static readonly EXPERIENCE_POTION;
        static readonly FALLING_BLOCK;
        static readonly FIREBALL;
        static readonly FISHING_HOOK;
        static readonly GHAST;
        static readonly IRON_GOLEM;
        static readonly ITEM;
        static readonly LAVA_SLIME;
        static readonly LIGHTNING_BOLT;
        static readonly MINECART;
        static readonly MUSHROOM_COW;
        static readonly OCELOT;
        static readonly PAINTING;
        static readonly PIG;
        static readonly PIG_ZOMBIE;
        static readonly PLAYER;
        static readonly PRIMED_TNT;
        static readonly RABBIT;
        static readonly SHEEP;
        static readonly SILVERFISH;
        static readonly SKELETON;
        static readonly SLIME;
        static readonly SMALL_FIREBALL;
        static readonly SNOWBALL;
        static readonly SNOW_GOLEM;
        static readonly SPIDER;
        static readonly SQUID;
        static readonly THROWN_POTION;
        static readonly VILLAGER;
        static readonly WOLF;
        static readonly ZOMBIE;
        static readonly ZOMBIE_VILLAGER;
    }

    class EntityRenderType {
        static readonly arrow;
        static readonly bat;
        static readonly blaze;
        static readonly boat;
        static readonly camera;
        static readonly chicken;
        static readonly cow;
        static readonly creeper;
        static readonly egg;
        static readonly enderman;
        static readonly expPotion;
        static readonly experienceOrb;
        static readonly fallingTile;
        static readonly fireball;
        static readonly fishHook;
        static readonly ghast;
        static readonly human;
        static readonly ironGolem;
        static readonly item;
        static readonly lavaSlime;
        static readonly lightningBolt;
        static readonly map;
        static readonly minecart;
        static readonly mushroomCow;
        static readonly ocelot;
        static readonly painting;
        static readonly pig;
        static readonly player;
        static readonly rabbit;
        static readonly sheep;
        static readonly silverfish;
        static readonly skeleton;
        static readonly slime;
        static readonly smallFireball;
        static readonly snowGolem;
        static readonly snowball;
        static readonly spider;
        static readonly squid;
        static readonly thrownPotion;
        static readonly tnt;
        static readonly unknownItem;
        static readonly villager;
        static readonly villagerZombie;
        static readonly witch;
        static readonly wolf;
        static readonly zombie;
        static readonly zombiePigman;
    }

    class ArmorType {
        static readonly boots;
        static readonly chestplate;
        static readonly helmet;
        static readonly leggings;
    }

    class MobEffect {
        static readonly absorption;
        static readonly blindness;
        static readonly confusion;
        static readonly damageBoost;
        static readonly damageResistance;
        static readonly digSlowdown;
        static readonly digSpeed;
        static readonly effectIds;
        static readonly fireResistance;
        static readonly harm;
        static readonly heal;
        static readonly healthBoost;
        static readonly hunger;
        static readonly invisibility;
        static readonly jump;
        static readonly movementSlowdown;
        static readonly movementSpeed;
        static readonly nightVision;
        static readonly poison;
        static readonly regeneration;
        static readonly saturation;
        static readonly waterBreathing;
        static readonly weakness;
        static readonly wither;
    }

    class DimensionId {
        static readonly NETHER;
        static readonly NORMAL;
    }

    class BlockFace {
        static readonly DOWN;
        static readonly EAST;
        static readonly NORTH;
        static readonly SOUTH;
        static readonly UP;
        static readonly WEST;
    }

    class UseAnimation {
        static readonly bow;
        static readonly normal;
    }

    class Enchantment {
        static readonly AQUA_AFFINITY;
        static readonly BANE_OF_ARTHROPODS;
        static readonly BLAST_PROTECTION;
        static readonly DEPTH_STRIDER;
        static readonly EFFICIENCY;
        static readonly FEATHER_FALLING;
        static readonly FIRE_ASPECT;
        static readonly FIRE_PROTECTION;
        static readonly FLAME;
        static readonly FORTUNE;
        static readonly INFINITY;
        static readonly KNOCKBACK;
        static readonly LOOTING;
        static readonly LUCK_OF_THE_SEA;
        static readonly LURE;
        static readonly POWER;
        static readonly PROJECTILE_PROTECTION;
        static readonly PROTECTION;
        static readonly PUNCH;
        static readonly RESPIRATION;
        static readonly SHARPNESS;
        static readonly SILK_TOUCH;
        static readonly SMITE;
        static readonly THORNS;
        static readonly UNBREAKING;
    }

    class EnchantType {
        static readonly all;
        static readonly axe;
        static readonly book;
        static readonly bow;
        static readonly fishingRod;
        static readonly flintAndSteel;
        static readonly hoe;
        static readonly pickaxe;
        static readonly shears;
        static readonly shovel;
        static readonly weapon;
    }

    class BlockRenderLayer {
        static readonly alpha;
        static readonly alpha_seasons;
        static readonly alpha_single_side;
        static readonly blend;
        static readonly doubleside;
        static readonly far;
        static readonly opaque;
        static readonly opaque_seasons;
        static readonly seasons_far;
        static readonly seasons_far_alpha;
        static readonly water;
    }

    var java: any;
    var android: any;
    var com: any;
}

export {};